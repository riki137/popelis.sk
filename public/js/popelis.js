/**
 * Dotwork canvas animation
 * @author Richard Popelis <richard@popelis.sk>
 */
var Dotwork = {
    /** @type HTMLCanvasElement */
    cvs: document.getElementById('bg'),
    dots: [],
    /** @type CanvasRenderingContext2D */
    ctx: null,
    resizeThrottler: null,

    newDot: function () {
        var width = Dotwork.cvs.width;
        var height = Dotwork.cvs.height;
        return {
            x: Math.random() * width,
            y: Math.random() * height,
            speedX: (Math.random() * 2) - 1,
            speedY: (Math.random() * 2) - 1,
            radius: 2 + (Math.random() * 2),
            boundDot: null,
            boundStrength: 0
        }
    },

    newDots: function () {
        var amount = (Dotwork.cvs.width * Dotwork.cvs.height) * .000133;
        var dots = [];
        for (var i = 0; i < amount; i++) {
            dots.push(this.newDot());
        }
        return dots;
    },

    drawDot: function (dot) {
        var ctx = this.ctx;
        ctx.fillStyle = '#dae0e4';
        ctx.beginPath();
        ctx.arc(dot.x, dot.y, dot.radius, 0, 2 * Math.PI);
        ctx.fill();
    },

    moveDot: function (dot) {
        // magnety (odtahovanie sa od najblizsieho bodu)
        if (dot.boundDot !== null) {
            if (dot.x > dot.boundDot.x) {
                dot.speedX += .05;
            } else {
                dot.speedX -= .05;
            }
            if (dot.y > dot.boundDot.y) {
                dot.speedY += .05;
            } else {
                dot.speedY -= .05;
            }
        }

        // odrazenie od rohov
        var width = Dotwork.cvs.width;
        var height = Dotwork.cvs.height;
        if (dot.x < 0 || dot.y < 0 || dot.x > width || dot.y > height) {
            if (dot.x < 0) {
                dot.speedX += .1;
            } else if (dot.x > width) {
                dot.speedX -= .1;
            }
            if (dot.y < 0) {
                dot.speedY += .1;
            } else if (dot.y > height) {
                dot.speedY -= .1;
            }
        }

        // limitacia rychlosti
        if (dot.speedX > 1 || dot.speedX < -1) {
            dot.speedX *= 0.99;
        }
        if (dot.speedY > 1 || dot.speedY < -1) {
            dot.speedY *= 0.99;
        }


        // zaklad pohybu
        dot.x += dot.speedX;
        dot.y += dot.speedY;
    },

    drawDotLines: function (dot) {
        var smallestDistance = false;
        var closestDot = null;
        var dots = Dotwork.dots;
        for (var i = 0; i < dots.length; i++) {
            if (dots[i] === dot) {
                continue;
            }
            var distance = Math.sqrt(
                Math.pow(dot.x - dots[i].x, 2) +
                Math.pow(dot.y - dots[i].y, 2)
            );
            if (smallestDistance === false || distance < smallestDistance) {
                closestDot = dots[i];
                smallestDistance = distance;
            }
        }

        if (dot.boundDot === closestDot) {
            if (dot.boundStrength < 1000) {
                dot.boundStrength++;
            }
        } else {
            dot.boundDot = closestDot;
            dot.boundStrength = 0;
        }

        var ctx = Dotwork.ctx;
        ctx.beginPath();
        ctx.moveTo(dot.x, dot.y);
        ctx.lineTo(closestDot.x, closestDot.y);
        var opacity = (dot.boundStrength / 1000) * (50 / smallestDistance);
        if (opacity > .3) {
            opacity = .3;
        }
        Dotwork.ctx.strokeStyle = 'rgba(0,0,0,' + opacity + ')';
        ctx.stroke();
    },

    drawFrame: function () {
        Dotwork.ctx.clearRect(0, 0, Dotwork.cvs.width, Dotwork.cvs.height);
        var dots = Dotwork.dots;
        for (var i = 0; i < dots.length; i++) {
            Dotwork.moveDot(dots[i]);
            Dotwork.drawDot(dots[i]);
            Dotwork.drawDotLines(dots[i]);
        }
    },

    resizeCanvas: function () {
        Dotwork.cvs.width = window.innerWidth;
        Dotwork.cvs.height = window.innerHeight;
        Dotwork.dots = Dotwork.newDots();
    },

    init: function () {
        this.ctx = this.cvs.getContext('2d');

        window.addEventListener('resize', function () {
            Dotwork.dots = [];
            if (Dotwork.resizeThrottler) {
                clearTimeout(Dotwork.resizeThrottler);
            }
            Dotwork.resizeThrottler = setTimeout(Dotwork.resizeCanvas, 50);
        }, false);
        Dotwork.resizeCanvas();

        setInterval(function () {
            window.requestAnimationFrame(Dotwork.drawFrame)
        }, 50)
    }
};

var Mail = {
    init: function () {
        var $mail = document.getElementById('mail');
        var $at = document.getElementById('t-at');
        var $dot = document.getElementById('t-dot');
        $mail.addEventListener('click', function () {
            $at.classList.remove('hidden');
            $dot.classList.add('hidden');
            $at.innerHTML = '@';
        });

        var listener = function () {
            if (this.classList.contains('hidden')) {
                this.innerHTML = '';
            }
        };
        $dot.addEventListener("transitionend", listener, false);
        $at.addEventListener("transitionend", listener, false);
        window.onblur = function () {
            $at.classList.add('hidden');
            $dot.classList.remove('hidden');
            $dot.innerHTML = '.';
        };
    }
};

Mail.init();
Dotwork.init();
